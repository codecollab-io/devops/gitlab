## GitLab

Terraform configuration for GitLab. This configuration handles

- GitLab CI Service Account on Google Cloud
- Group variables for GitLab CI Service Account on GitLab
- Group variables for group Deploy Token

### Note

Deploy keys for repositories under the Protocol Buffers subgroup are not managed by Terraform