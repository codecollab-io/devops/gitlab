terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "3.7.0"
    }
  }
}

variable "GITLAB_ACCESS_TOKEN" {
  type = string
}

data "terraform_remote_state" "iam" {
  backend = "remote"

  config = {
    organization = "codecollab-io"
    workspaces = {
      name = "iam-production"
    }
  }
}

provider "gitlab" {
  token = var.GITLAB_ACCESS_TOKEN
}

# Add service account CI/CD variables
resource "gitlab_group_variable" "service_account" {
  group = "codecollab-io"
  key = "CI_SERVICE_ACCOUNT"
  value = data.terraform_remote_state.iam.outputs.gitlab_ci_account
}

# Add service key to CI/CD variables
resource "gitlab_group_variable" "service_key" {
  group = "codecollab-io"
  key = "CI_SERVICE_KEY"
  value = base64decode(data.terraform_remote_state.iam.outputs.gitlab_ci_key)
}

# Create Deploy Token
resource "gitlab_deploy_token" "gitlab_ci" {
  group = "codecollab-io"

  name = "GitLab CI"
  username = "gitlab-ci"

  scopes = [
    "read_repository"
  ]
}

# Add Deploy Token to CI/CD variables
resource "gitlab_group_variable" "deploy_username" {
  group = "codecollab-io"

  key = "CI_DEPLOY_USERNAME"
  value = gitlab_deploy_token.gitlab_ci.username
}

resource "gitlab_group_variable" "deploy_token" {
  group = "codecollab-io"

  key = "CI_DEPLOY_TOKEN"
  value = gitlab_deploy_token.gitlab_ci.token
}

